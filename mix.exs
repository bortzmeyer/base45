defmodule Base45.MixProject do
  use Mix.Project

  def project do
    [
      app: :base45_elixir,
      name: "base45",
      description: "Encode and decode data following the Base45 standard (RFC 9285)",
      version: "0.1.2",
      elixir: "~> 1.9",
      source_url: "https://framagit.org/bortzmeyer/base45",
      homepage_url: "http://hexdocs.pm/base45_elixir",
      start_permanent: Mix.env() == :prod,
      package: package(),
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false}, # For Dialyzer static analysis https://hexdocs.pm/dialyxir/
      {:ex_doc, "~> 0.27", only: :dev, runtime: false} # For documentation https://github.com/elixir-lang/ex_doc
    ]
  end

  defp package do
    [
      files: ~w(lib test *.exs README.md LICENSE),
      licenses: ["BSD-3-Clause"],
      links: %{"FramaGit" => "https://framagit.org/bortzmeyer/base45"}
    ]
  end
  
end
