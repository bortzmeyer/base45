defmodule Base45Test do
  use ExUnit.Case
  doctest Base45

  # Tests in the RFC
  test "encode1" do
    assert Base45.encode("AB") == "BB8"
  end
  test "encode2" do
    assert Base45.encode("Hello!!") == "%69 VD92EX0"
  end
  test "encode3" do
    assert Base45.encode("base-45") == "UJCLQE7W581"
  end
  test "encode4" do
    assert Base45.encode("") == ""
  end
  
  test "encodebinary1" do
    assert Base45.encode(<<0, 255>>) == "U50"
  end
  test "encodebinary2" do
    assert Base45.encode(<<1>>) == "10"
  end
  test "encodebinary3" do
    assert Base45.encode(<<1, 2>>) == "X50"
  end
  test "encodebinary4" do
    assert Base45.encode(<<1, 2, 3>>) == "X5030"
  end
  test "encodebinary5" do
    assert Base45.encode(<<1, 2, 3, 4>>) == "X507H0"
  end

  # Tests in the RFC
  test "decode1" do
    assert Base45.decode("QED8WEX0") == "ietf!"
  end
  test "decode2" do
    assert Base45.decode("%69 VD92EX0") == "Hello!!"
  end
  test "decode3" do
    assert Base45.decode("BB8") == "AB"
  end
  test "decode4" do
    assert Base45.decode("QED8WEX0") == "ietf!"
  end
  test "decode5" do
    assert Base45.decode(".Y80FD*EDH44.OEM3DX CQ2") == "Elixir is great"
  end
  test "decode6" do
    assert Base45.decode("3H0") == <<3, 0>>
  end
  test "decode7" do
    assert Base45.decode("") == ""
  end
  test "decode8" do
    assert Base45.decode("200") == <<0, 2>>
  end
  test "decode9" do
    assert Base45.decode("20") == <<2>>
  end
  # Example in the RFC
  test "decode10" do
    assert Base45.decode("FGW") == <<255, 255>>
  end
  test "decodeerror1" do
    assert Base45.decode("=") == nil
  end
  test "decodeerror2" do
    assert Base45.decode("QED8WEX0=") == nil
  end
  test "decodeerror3" do
    assert Base45.decode("QeD8") == nil
  end
  test "decodeerror4" do # All characters allowed but invalid nevertheless
    assert Base45.decode("T:6UDB6OV8+3T") == nil
  end
  test "decodeerror5" do # All characters allowed but invalid nevertheless
    assert Base45.decode("TL$N9P9:X:G05JJ.TH") == nil
  end
  
  test "decode!1" do
    assert Base45.decode!("QED8WEX0") == "ietf!"
  end
  test "decode!2" do
    assert Base45.decode!("%69 VD92EX0") == "Hello!!"
  end
  test "decode!3" do
    assert Base45.decode!("BB8") == "AB"
  end
  test "decode!4" do
    assert Base45.decode!("QED8WEX0") == "ietf!"
  end
  test "decode!5" do
    assert Base45.decode!(".Y80FD*EDH44.OEM3DX CQ2") == "Elixir is great"
  end
  test "decode!6" do
    assert Base45.decode!("3H0") == <<3, 0>>
  end
  test "decode!7" do
    assert Base45.decode!("") == ""
  end
  test "decode!8" do
    assert Base45.decode!("200") == <<0, 2>>
  end
  test "decode!9" do
    assert Base45.decode!("20") == <<2>>
  end
  test "decode!error1" do
    assert_raise NilValuesError, fn -> Base45.decode!("=") end
  end
  test "decode!error2" do
    assert_raise NilValuesError, fn -> Base45.decode!("QED8WEX0=") end
  end
  # Example in the RFC
  test "decode!error3" do
    assert_raise InvalidBase45Error, fn -> Base45.decode!("GGW") end
  end

  test "roundtrip1" do
    tests = ["foo", "bar", <<5, 6, 99>>, <<178, 23, 0>>, <<54, 2, 1, 89>>, <<1, 2, 5, 255, 69, 0>>,
	     <<0, 2>>,
	     <<47, 225, 206, 65, 0, 81>>]
      Enum.map(tests, fn s -> assert Base45.decode(Base45.encode(s)) == s end)
  end

  def random_binary_list() do
    length = :rand.uniform(30)
    Enum.map(1..length, fn _i -> :rand.uniform(256)-1 end) # We could
    # use :rand.bytes, also, but it appeared only with Erlang >= 24
    |> Enum.map_reduce(<<>>, fn c, acc -> {nil, acc <> <<c>>} end)
    |> elem(1)
  end
  
  def random_string() do
    length = :rand.uniform(30)
    Enum.map(1..length, fn _i -> :rand.uniform(map_size(Base45.value_to_code))-1 end)
    |> Enum.map_reduce("", fn c, acc -> {nil, acc <> Base45.value_to_code[c]} end)
    |> elem(1)
  end
  
  # Check on random data that encode|>decode is the identity function
  test "roundtrip2" do
    Enum.map(1..10000, fn _x ->
      s = random_binary_list()
      assert Base45.decode(Base45.encode(s)) == s
    end)
  end

  # We do not check on random data that decode|>encode is the identity
  # function since several Base45 strings may encode the same binary
  # content.
  #test "roundtrip3" do
  #  Enum.map(1..10000, fn _x -> 
  #    s = random_string()
  #    # These random strings may perfectly well not be proper Base45
  #    assert Base45.decode(s) == nil or Base45.encode(Base45.decode(s)) == s
  #  end)
  #end

  @input_file "test/640px-Borobudur-Temple-Park_Elephant-cage-01.jpg"
  @output_file "/tmp/base45-test-binary-file.data"
  test "file" do
    file = File.open(@input_file, [:read, :binary])
    f = case file do
	  {:ok, handle} -> handle
	  {:error, reason} -> raise "Cannot read file #{@input_file}: #{reason}"
	end
    e = Base45.encode_file(f)
    assert e != nil
    result = File.write(@output_file, Base45.decode!(e), [:write])
    case result do
      :ok -> nil
      {:error, reason} -> raise "Cannot write file #{@output_file}: #{reason}"
    end
    # TODO check the files are identical (they are, we tested with
    # diff, but check here)
  end
  
end
