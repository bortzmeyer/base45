import random
import subprocess
import sys

# https://github.com/kirei/python-base45
import base45

gen = random.Random()

for i in range(0, 100):
    number = gen.randrange(0, 30)
    data = b''
    for j in range(0, number):
        data += gen.randint(0, 255).to_bytes(1, 'big')
    e = base45.b45encode(data)
    p = subprocess.Popen(["mix", "run", "decode.exs"], stdin=subprocess.PIPE,
                     stdout=subprocess.PIPE)
    result = p.communicate(e)
    if result[0] != data:
        print("Something is terribly wrong")
        sys.exit(1)
    p = subprocess.Popen(["mix", "run", "encode.exs"], stdin=subprocess.PIPE,
                     stdout=subprocess.PIPE)
    result = p.communicate(data)
    encoded = result[0][:-1] # Chop the ending end-of-line
    d = base45.b45decode(encoded)
    if d != data:
        print("Something is terribly wrong")
        sys.exit(1)
